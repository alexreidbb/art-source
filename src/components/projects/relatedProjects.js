import React from 'react'
import { useStaticQuery, graphql, Link } from 'gatsby'
import '../../styles/projects/project-related.scss'
import FadeInWhenVisible from '../fadeInView';
import { GatsbyImage } from "gatsby-plugin-image"


const Related = ({title}) => {

    const data = useStaticQuery(graphql`
    query relatedProjects {
        allWpPost {
          edges {
            node {
              featuredImage {
                node {
                  localFile {
                    childImageSharp {
                      gatsbyImageData(
                        quality: 82,
                        formats: [WEBP]
                      )
                    }
                  }
                }
              }
              title
              link
              project {
                artWorkDetails
                artistName {
                  name
                }
              }
              categories {
                nodes {
                  name
                }
              }
            }
          }
        }
      }
      
    `)


    let projects = data.allWpPost.edges;
    const updatedProjects = projects.filter(project => title !== project.node.title );

    return (  
      <FadeInWhenVisible>
        <div className="related">
            <h1 className='font__large font__italic related__title'>Related projects</h1>
            <div className="related__container">


            {updatedProjects.map((project) => {
              
                    return <div className="related__project" key={project.node.title}>

                        <Link to={'/projects' + project.node.link}>
                            <figure>
                                {project.node.featuredImage.node && <GatsbyImage image={project.node.featuredImage.node.localFile.childImageSharp.gatsbyImageData} alt="project art work" className='related__img' />}
                            </figure>
                            <p className='font__large'>{project.node.project.artistName[0].name}</p>
                            <p className='font__mid font__italic'>{project.node.title}</p>
                        </Link>

                    </div>
                
            })}

            </div>
        </div>
      </FadeInWhenVisible>
    );
}
 
export default Related;