

const RemoveDuplicates = (data) => {
    const allPostCodes = [];

    data.forEach(element => {
        const postCode = element.node.project.mapPostCode;
        allPostCodes.push(postCode);
    })

    const set = new Set(allPostCodes);
    const updatedPostCodes = [...set];
    
    return updatedPostCodes;
}

export default RemoveDuplicates;