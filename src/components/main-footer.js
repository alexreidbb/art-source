import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import '../styles/footer.scss'

const Footer = () => {
    const data = useStaticQuery(graphql`
    query FooterQuery {
        allWpPage(filter: {slug: {eq: "contact"}}) {
            nodes {
                contact {
                  address
                  email
                  pageText
                  phone
                  instagramLink
                  instagramName
                }
              }
            }
          }
      `)

      const item = data.allWpPage.nodes[0].contact;

    return(

          <section className="footer-content">
               
            <div className="footer-item">
              <a href='https://goo.gl/maps/NXDUtGb8UuPUUmB79' target="_blank" rel="noreferrer">
                <h2 className="font__small">Address</h2>
                <div className="footer-break"></div>
                <p className="font__x-small font__sans" dangerouslySetInnerHTML={{__html: item.address}} />
                </a>
            </div>
            <div className="footer-item">
              <a href={'mailto:' + item.email } target="_blank" rel="noreferrer">
                <h2 className="font__small">Email</h2>
                <div className="footer-break"></div>
                <p className="font__x-small font__sans">{item.email}</p>
              </a>
            </div>
            <div className="footer-item">
              <a href={ item.instagramLink } target="_blank" rel="noreferrer">
                <h2 className="font__small">Instagram</h2>
                <div className="footer-break"></div>
                <p className="font__x-small font__sans">{item.instagramName}</p>
              </a>
            </div>
            <div className="footer-item">
              <a href={'tel:' + item.phone } target="_blank" rel="noreferrer">
                <h2 className="font__small">Phone</h2>
                <div className="footer-break"></div>
                <p className="font__x-small font__sans">{item.phone}</p>
              </a>
            </div>

          </section>

    )
}

export default Footer