import React from 'react'
import '../../styles/projects/project-intro.scss'
import { motion } from 'framer-motion';
import { GatsbyImage } from 'gatsby-plugin-image';

const Intro = ({ img, workTitle, mainText, bioText, header }) => {

    return (  

        <motion.div className="project__intro"
        initial={{opacity: 0, y: '50px'}}
        animate={{opacity: 1, y: 0}}
        transition={{ duration: 1,  delay: 0.2}}>
            { img && <figure>
                
                {img.node && <GatsbyImage image={img.node.localFile.childImageSharp.gatsbyImageData} alt="artWork" /> }
            </figure>}
            <div className="project__intro--text">
                { header && <h2 className="font__large">{header}</h2>}
                { workTitle && <p className="font__mid sub-heading project__intro--sub-head">{workTitle}</p>}
                { mainText && <div className="font__text" dangerouslySetInnerHTML={{__html: mainText }} />}
                { bioText && <div className="project__intro--bio-line"></div>}
                { bioText && <p className="font__text project__intro--bio">Artist Bio</p>}
                { bioText && <div className="font__text project__intro--bio-text" dangerouslySetInnerHTML={{__html: bioText }}  />}
                <p className="font__mid sub-heading project__intro--more">More information below</p>
            </div>
        </motion.div>

    );
}
 
export default Intro;