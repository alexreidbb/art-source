const path = require('path')

exports.createPages = async ({ graphql, actions }) => {

    const result = await graphql(`
    query PostsQuery {
        allWpPost {
          nodes {

            slug
          }
        }
      }
    `)

    const { allWpPost } = result.data
    
    if (allWpPost.nodes.length) {
      allWpPost.nodes.map(post => {
        actions.createPage({
            path: '/projects/' + post.slug,
            component: path.resolve('./src/templates/project-details.js'),
            context:  { slug: post.slug }
        })
      })
    }
}