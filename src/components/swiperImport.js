import "swiper/css";
import "swiper/css/effect-fade"
import "swiper/css/navigation"
import "swiper/css/pagination"
import SwiperCore, { EffectFade ,Navigation, Autoplay,Pagination } from 'swiper';


SwiperCore.use([Autoplay, EffectFade,Navigation,Pagination]);