import React from 'react'
import { Link } from "gatsby";

const Navbar = () => {
    return (  
        <nav>
            <div className="links">
                <Link to="/projects" activeClassName='activeLink'><p className="font__mid">Projects</p></Link>
                <Link to="/about" activeClassName='activeLink'><p className="font__mid">About</p></Link>
                <Link to="/contact" activeClassName='activeLink'><p className="font__mid">Contact</p></Link>
            </div>
        </nav>
    );
}
 
export default Navbar;