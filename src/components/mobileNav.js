import React from 'react'
import Navbar from './navbar'
import { motion, AnimatePresence } from 'framer-motion';
import Logo from '../images/Art_Source_Black_logo.svg';


// Animations

const containerVariants = {
    hidden: {
      opacity: 0
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 0.4, 
        type: 'tween',
        when: "beforeChildren",
        staggerChildren: 0.6
      }
    }
  }

  const childVariants = {
    hidden: {
      y: '30px',
      opacity: 0,
      
    },
    visible: {
      y: 0,
      opacity: 1,
      
    }
  }

const MobileNav = () => {
    return (
        <AnimatePresence>
        <motion.div className="mobileNav"
        variants={containerVariants}
        initial="hidden"
        animate="visible"
        exit={{
            opacity: 0
        }}>

          <div className="mobileNav__logo">
            <a href="/" className="main-logo">
                <img src={Logo} alt="logo" />
            </a>
          </div>

          <motion.div className="mobileNav__content" variants={childVariants}
          transition={{
              delay: 0.2,
              duration: 0.6, 
              type: 'tween',
          }}>
            <Navbar />
          </motion.div>

        </motion.div>
        </AnimatePresence>
    );
}
 
export default MobileNav;